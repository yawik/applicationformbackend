<?php


\ApplicationFormBackend\Module::$isLoaded = true;

/**
 * create a config/autoload/YawikDemoSkin.global.php and put modifications there
 */

return array(
    'service_manager' => [
        'factories' => [
            'Auth/Dependency/Manager' => 'ApplicationFormBackend\Factory\Dependency\ManagerFactory',
        ],
    ],
    'view_manager' => [
        'template_map' => [
        'startpage' => __DIR__ . '/../view/layout.phtml',
        'layout/layout' => __DIR__ . '/../view/layout.phtml',
        'core/index/index' => __DIR__ . '/../view/index.phtml',
        'form2mail/mail/sendmail' => __DIR__ . '/../view/mail/sendmail.phtml',
        'form2mail/mail/conduent' => __DIR__ . '/../view/mail/conduent.phtml',
        'form2mail/mail/conduent-confirm' => __DIR__ . '/../view/mail/conduent-confirm.phtml',
        'form2mail/mail/invite-recruiter' => __DIR__ . '/../view/mail/invite-recruiter.phtml',
        'form2mail/mail/invite-recruiter-text' => __DIR__ . '/../view/mail/invite-recruiter-text.phtml',
        'form2mail/mail/header' => __DIR__ . '/../view/mail/header.phtml',
        'form2mail/mail/footer' => __DIR__ . '/../view/mail/footer.phtml',
        'mail/forgotPassword' =>  __DIR__ . '/../view/mail/forgot-password.phtml',
        'piwik' => __DIR__ . '/../view/piwik.phtml',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ],
        ],
    ],
    'form_elements' => [
        'invokables' => [
            'Jobs/Description' => 'ApplicationFormBackend\Form\JobsDescription',
        ],
    ],

    'acl' => [
        'rules' => [
            'guest' => [
                'deny' => [
                    'route/lang/jobs',
                    'Jobboard',
                ],
            ],
        ]
    ],

);
