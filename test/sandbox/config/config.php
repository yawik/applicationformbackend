<?php

chdir(dirname(__DIR__));
return [
    'modules' => [
        'Core',
        'Auth',
        'Jobs',
        'Settings',
        'Cv',
        'Applications',
        'Organizations',
        'Geo',
        'SlmQueue',
        'Form2Mail',
        'ApplicationFormBackend'
    ],
];
