#!/bin/sh

MJML=./node_modules/.bin/mjml

for i in sendmail forgot-password invite-recruiter conduent
do
  output=view/mail/${i}.phtml;
  echo "build $output"
  $MJML view/mail-src/${i}.mjml -o $output;
done