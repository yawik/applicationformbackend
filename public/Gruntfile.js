module.exports = function(grunt) {
    var targetDir = grunt.config.get('targetDir');
    var nodeModulesPath = grunt.config.get('nodeModulesPath');

    grunt.config.merge({
        less: {
            applicationformbackend: {
                options: {
                    compress: false,
                    modifyVars: {
                        "fa-font-path": "/dist/fonts",
                        "flag-icon-css-path": "/dist/flags"
                    }
                },
                files: [
                    {
                        src: [
                            targetDir+"/modules/ApplicationFormBackend/less/layout.less",
                            "./node_modules/select2/dist/css/select2.min.css",
                            "./node_modules/pnotify/dist/pnotify.css",
                            "./node_modules/pnotify/dist/pnotify.buttons.css",
                            "./node_modules/bootsrap3-dialog/dist/css/bootstrap-dialog.css"
                        ],
                        dest: targetDir+"/modules/ApplicationFormBackend/dist/layout.css"
                    }
                ]
            },
        },
        cssmin: {
            applicationformbackend: {
                files: [
                    {
                        dest: targetDir+'/modules/ApplicationFormBackend/dist/layout.min.css',
                        src: targetDir+'/modules/ApplicationFormBackend/dist/layout.css'
                    }
                ]
            }
        }
    });

    grunt.registerTask('yawik:applicationformbackend',['copy','less','concat','uglify','cssmin']);
};