<?php
namespace Deployer;

require 'recipe/zend_framework.php';

// Project name
set('application', 'ApplicationFormBackend');

// Project repository
set('repository', 'https://gitlab.com/yawik/applicationformbackend.git');

// Shared files/dirs between deploys
add('shared_files', [
    'test/sandbox/public/.htaccess',
    'test/sandbox/public/robots.txt',
]);

add('shared_dirs', [
    'test/sandbox/var/log',
    'test/sandbox/var/cache',
    'test/sandbox/config/autoload',
    'test/sandbox/public/static',	// used by eg. organization logos
]);

// Writable dirs by web server
add('writable_dirs', [
    'test/sandbox/var/cache',
    'test/sandbox/var/log',
    'test/sandbox/public/static',
]);

set('default_stage', 'prod');

// Hosts

host('form2mail.yawik.org')
    ->user('yawik')
    ->stage('prod')
    ->multiplexing(false)
    ->set('deploy_path', '/var/www/production')
    ->set('writableusesudo', true);

host('staging.form2mail.yawik.org')
    ->user('yawik')
    ->stage('staging')
    ->multiplexing(false)
    ->set('deploy_path', '/var/www/staging')
    ->set('writableusesudo', true);


// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// restart php7.4-fpm.
// Requires configuration of /etc/sudoers.d/yawik on the target system
after('cleanup', 'restart');

task('restart', '
    sudo /bin/systemctl restart php7.4-fpm.service;
    sudo /bin/systemctl restart php7.4-staging-fpm.service;
    ');
